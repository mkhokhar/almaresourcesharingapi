from django import forms


BOOK_OR_ARTICLE_CHOICES = (
    ('book', 'Book'),
    ('article', 'Article'),
)

FORMAT_CHOICES = (
    ('PHYSICAL', 'Print'),
    ('DIGITAL', 'Digital'),
)


class ResourceSharingRequestForm(forms.Form):
    # Generic user fields
    library_card_number = forms.CharField(widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder':'University card number'}))
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Name'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Email address'}))

    # General bibliographic fields
    title = forms.CharField(widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Title of article/book'}))
    author = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Author name'}))
    place_of_publication = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Place of publication'}))
    year = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Year of publication'}))
    notes = forms.CharField(required=False,widget=forms.Textarea(attrs={'class':'input-xxlarge'}))
    pages = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Pages'}))

    # Article specific fields
    journal_title = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Title of journal'}))
    volume = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Volume'}))
    issue = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Issue number'}))
    issn = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'ISSN'}))

    # Book specific fields
    publisher = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Publisher'}))
    edition = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Edition'}))
    isbn = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'ISBN'}))
    chapter = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Chapter'}))
    classmark = forms.CharField(required=False, widget=forms.TextInput(attrs={'class':'input-xxlarge', 'placeHolder': 'Classmark'}))

    # Request specific fields
    format = forms.ChoiceField(choices=FORMAT_CHOICES, widget=forms.Select(attrs={'class':'input-xxlarge'}))
    not_required_after = forms.CharField(widget=forms.TextInput(attrs={'class':'input-xxlarge'}))
    copyright_agreement = forms.BooleanField()


class BookOrArticleForm(forms.Form):
    book_or_article = forms.ChoiceField(choices=BOOK_OR_ARTICLE_CHOICES, widget=forms.Select(attrs={'class':'input-xxlarge'}))