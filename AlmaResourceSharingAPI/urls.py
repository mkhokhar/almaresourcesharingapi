from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'AlmaResourceSharingAPI.views.home', name='home'),
    url(r'^process/complete$', 'AlmaResourceSharingAPI.views.process_complete', name='complete'),
    url(r'^process/(?P<request_type>(article|book))/$', 'AlmaResourceSharingAPI.views.process_resource_sharing_request_form', name='process'),
    # url(r'^AlmaResourceSharingAPI/', include('AlmaResourceSharingAPI.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
