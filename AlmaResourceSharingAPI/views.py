from django.shortcuts import render, redirect
from AlmaResourceSharingAPI.forms import ResourceSharingRequestForm, BookOrArticleForm
from lxml import etree
from AlmaResourceSharingAPI.settings import API_address, API_password, API_username
from AlmaResourceSharingAPI.settings import AlertEmail_subject, AlertEmail_message_text, AlertEmail_message_html, AlertEmail_from
from AlmaResourceSharingAPI.settings import PRIMO_URL, PRIMO_DISPLAY_URL
import requests
from string import Template
from django.core.mail import EmailMultiAlternatives
import urllib



class ResourceSharingRequest:
    def __init__(self):
        pass

    def generate_payload(self):
        root = etree.Element("user_resource_sharing_request")

        for key, value in self.__dict__.items():

            if key == 'library_card_number' or key == 'name':
                continue

            element = etree.Element(key)
            element.text = value

            if key == 'format':
                element.set("desc", value)

            if key == 'citation_type':
                if value == 'BK':
                    element.set('desc', 'Book')
                else:
                    element.set('desc', 'Article')

            root.append(element)

        masud = etree.tostring(root)
        print etree.tostring(root)
        return etree.tostring(root)

    def primo_search(self):
        # Primo default name space
        DEFAULT_NS = 'http://www.exlibrisgroup.com/xsd/primo/primo_nm_bib'

        # We test for the exact title value
        query = 'title,exact,' + self.title

        # We will look for at most 10 results
        url = PRIMO_URL + '&query=' + query + '&indx=1&bulkSize=10'

        content = urllib.urlopen(url)
        xml = etree.parse(content)

        docset = xml.getroot().xpath('//sear:SEGMENTS/sear:JAGROOT/sear:RESULT/sear:DOCSET', namespaces={'sear': 'http://www.exlibrisgroup.com/xsd/jaguar/search', 'def': DEFAULT_NS})
        totalhits = docset[0].get("TOTALHITS");
        docs = xml.getroot().xpath('//sear:SEGMENTS/sear:JAGROOT/sear:RESULT/sear:DOCSET/sear:DOC/def:PrimoNMBib/def:record', namespaces={'sear': 'http://www.exlibrisgroup.com/xsd/jaguar/search', 'def': DEFAULT_NS})

        list_data = []
        for doc in docs:
            data = {}
            data['totalhits'] = totalhits
            data['record_id'] = doc.findtext('{%s}control/{%s}recordid' %(DEFAULT_NS, DEFAULT_NS))
            data['title'] = doc.findtext('{%s}display/{%s}title' %(DEFAULT_NS, DEFAULT_NS))
            data['creationdate'] = doc.findtext('{%s}display/{%s}creationdate' %(DEFAULT_NS, DEFAULT_NS))
            data['publisher'] = doc.findtext('{%s}display/{%s}publisher' %(DEFAULT_NS, DEFAULT_NS))
            creator = doc.findtext('{%s}display/{%s}creator' %(DEFAULT_NS, DEFAULT_NS))
            contributor = doc.findtext('{%s}display/{%s}contributor' %(DEFAULT_NS, DEFAULT_NS))
            if creator:
                data['author'] = creator
            else:
                data['author'] = 'No author specified'
            data['type'] = doc.findtext('{%s}display/{%s}type' %(DEFAULT_NS, DEFAULT_NS))
            s = Template(PRIMO_DISPLAY_URL)
            link = s.substitute(record_id=data['record_id'])
            data['display_link'] = link
            list_data.append(data)

        return list_data


'''
    method: home
    author: Masud Khokhar
    parameters: request
    returns: article form or book form
    acceptable request methods: GET or POST
'''


def home(request):
    # If the request method is GET, then show the basic form for selection of Book or Article
    if request.method == 'GET':
        form = BookOrArticleForm()
        # Define the context for the render operation. This is additional values you want to pass to the template.
        context = {'BookOrArticleForm': form}
        return render(request, 'home.html', context)
    # If the request method is POST, then the user has selected either book or article. Make a decision on that choice.
    else:
        form = BookOrArticleForm(request.POST)
        if form.is_valid:
            # Grab the value from the request for the user's choice
            request_type = request.POST.get('book_or_article')

            return redirect('process', request_type=request_type)


'''
    method: process_resource_sharing_request_form
    author: Masud Khokhar
    parameters: request, request_type (article or book)
    returns:
    acceptable request methods: GET or POST
'''


def process_resource_sharing_request_form(request, request_type):
    if request.method == 'GET':
        form = ResourceSharingRequestForm()
        context = {'ResourceSharingRequestForm': form, 'request_type': request_type}
        return render(request, 'resource_sharing_request_form.html', context)
    else:
        form = ResourceSharingRequestForm(request.POST)

        if form.is_valid():
            # Create a new instance of ResourceSharingRequest
            resource_sharing_request = ResourceSharingRequest()

            # Assign values from the submitted form to the above created instance of ResourceSharingRequest
            # We will assign all values here regardless of whether it would be an article form or a book form

            # Generic user fields
            resource_sharing_request.library_card_number = form.cleaned_data['library_card_number']

            resource_sharing_request.name = form.cleaned_data['name']

            resource_sharing_request.text_email = form.cleaned_data['email']

            # General bibliographic fields
            resource_sharing_request.title = form.cleaned_data['title']

            resource_sharing_request.author = form.cleaned_data['author']

            resource_sharing_request.place_of_publication = form.cleaned_data['place_of_publication']

            resource_sharing_request.year = form.cleaned_data['year']

            resource_sharing_request.note = form.cleaned_data['notes']

            resource_sharing_request.pages = form.cleaned_data['pages']

            # Article specific fields
            resource_sharing_request.journal_title = form.cleaned_data['journal_title']

            resource_sharing_request.volume = form.cleaned_data['volume']

            resource_sharing_request.issue = form.cleaned_data['issue']

            resource_sharing_request.issn = form.cleaned_data['issn']

            # Book specific fields
            resource_sharing_request.publisher = form.cleaned_data['publisher']

            resource_sharing_request.edition = form.cleaned_data['edition']

            resource_sharing_request.isbn = form.cleaned_data['isbn']

            resource_sharing_request.chapter = form.cleaned_data['chapter']

            resource_sharing_request.call_number = form.cleaned_data['classmark']

            # Request specific fields
            resource_sharing_request.format = form.cleaned_data['format']

            resource_sharing_request.last_interest_date = form.cleaned_data['not_required_after']

            if form.cleaned_data['copyright_agreement']:
                resource_sharing_request.agree_to_copyright_terms = 'true'
            else:
                resource_sharing_request.agree_to_copyright_terms = 'false'

            if request_type == 'article':
                resource_sharing_request.citation_type = 'CR'
            else:
                resource_sharing_request.citation_type = 'BK'

            resource_sharing_request.use_alternative_address = 'true'

            resource_sharing_request.pickup_location = 'LANUL'

            payload = resource_sharing_request.generate_payload()

            # Definitely worth checking for this item in Primo and Primo Central
            primo_results = resource_sharing_request.primo_search()

            if primo_results:
                context = {'primo_results': primo_results, 'payload': payload}
                return render(request, 'exists.html', context)

            # Grab the logged in user name
            requesting_user = request.META.get('REMOTE_USER')

            # Place the API call to Alma
            response = place_API_call(payload, requesting_user, 'false')

            # Create an XML object from the API response for easy parsing
            response_xml = etree.fromstring(response)
            print response

            # Set the request ID to False and then check if there is one coming back from Alma
            request_id = False
            request_id = response_xml.findtext("request_id")

            # If a request ID has come back, then the request has been successfully placed
            if request_id:
                return render(request, 'success.html')
            # Otherwise, something has gone wrong, parse for error code and show the user a generic error page.
            else:
                alert_staff(resource_sharing_request.__dict__)
                return render(request, 'error.html')

        context = {'ResourceSharingRequestForm': form, 'request_type': request_type}
        return render(request, 'resource_sharing_request_form.html', context)


def process_complete(request):
    if request.method == 'GET':
        return redirect('home')
    if request.method == 'POST':

        payload = request.POST.get('payload')
        # Grab the logged in user name
        requesting_user = request.META.get('REMOTE_USER')

        # Place the API call to Alma
        response = place_API_call(payload, requesting_user, 'true')

        # Create an XML object from the API response for easy parsing
        response_xml = etree.fromstring(response)
        print response

        # Set the request ID to False and then check if there is one coming back from Alma
        request_id = False
        request_id = response_xml.findtext("request_id")

        # If a request ID has come back, then the request has been successfully placed
        if request_id:
            return render(request, 'success.html')
        # Otherwise, something has gone wrong, parse for error code and show the user a generic error page.
        else:
            return render(request, 'error.html')

def alert_staff(data):
    message_text = AlertEmail_message_text
    message_html = AlertEmail_message_html

    for key, value in data.iteritems():
        message_text += key + ': ' + data[key] + '\n'
        message_html += key + ': ' + data[key] + '<br/>'

    AlertEmail_to = data['text_email']
    msg = EmailMultiAlternatives(AlertEmail_subject, message_text, AlertEmail_from, [AlertEmail_to])
    msg.attach_alternative(message_html, "text/html")
    msg.send()


def place_API_call(payload, requesting_user, override_blocks):
    s = Template(API_address)

    if requesting_user is None:
        requesting_user = 'khokhamm'

    url = s.substitute(requesting_user=requesting_user, override_blocks=override_blocks)

    return requests.post(url, auth=(API_username, API_password), data=payload).content