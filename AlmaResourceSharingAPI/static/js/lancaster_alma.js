$(document).ready(function() {

    $('.input-append.date').datepicker({
        format: "yyyy-mm-dd",
        startDate: "now",
        orientation: "auto",
        autoclose: true
    });

    $('#id_notes').popover({
        'placement': 'right',
        'trigger': 'focus',
        'html': true,
        'content': 'Please add additional notes relevant for this request.<br/><br/>If your request is urgent, please use the word <strong>"urgent"</strong> in your notes.'
    })

    $('#id_edition').popover({
        'placement': 'left',
        'trigger': 'focus',
        'html': true,
        'content': ' If you would prefer a particular edition of a book, please enter it here, e.g. 3rd edition<br/><br/>If no other edition will do, please specify, e.g. 2nd edition ONLY.'
    })
})